import {combineReducers} from "redux";
import CartReducer from "./cartReducer";
import CardItemReducer from "./cardItemReducer";
import modalReducer from "./modalReducer";

const appReducer = combineReducers({
    cards: CardItemReducer,
    cart: CartReducer,
    modal: modalReducer,
})

export default appReducer;