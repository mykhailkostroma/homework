import React from 'react';
import styles from './BasketPage.module.scss'
import BasketItem from "../../components/BasketItem";
import {shallowEqual, useSelector} from "react-redux";
import {Button} from "@mui/material";
import Order from "../../components/Order";


const BasketPage = () => {

    const basketItems = useSelector(state => state.cart.cartItems, shallowEqual)

    return (
        <>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {basketItems && basketItems.map(item => <BasketItem key={item.id} {...item}/>)}
                {!basketItems.length && <h1>No items have been added :(</h1>}
            </div>
            <div style={{display: 'flex', justifyContent: 'center', marginTop: '30px'}}>
                {/*<Button variant="contained" size={"large"}>Buy</Button>*/}
            </div>

            {!!basketItems.length && <Order/>}
        </>

    );
};

export default BasketPage;