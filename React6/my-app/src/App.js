import styles from './App.module.scss';
import Header from "./components/Header/Header";
import {BrowserRouter} from "react-router-dom";
import {useEffect, useState} from "react";
import Routes from "./Routes";
import Modal from "./components/Modal";
import {Button} from "@mui/material";
import {Provider, useDispatch} from "react-redux";
import store from "./store";

function App() {

    // const [isOpenModal, setIsOpenModal] = useState(false);
    // const [currentName, setCurrentName] = useState("");
    // const [basket, setBasket] = useState([]);
    // const [isAddModal, setIsAddModal] = useState(true);



    // const openModal = (name, type = 'add') => {
    //     if (type === 'delete') {
    //         setIsAddModal(false);
    //     } else {
    //         setIsAddModal(true);
    //     }
    //
    //     setIsOpenModal(true)
    //     setCurrentName(name)
    // }

    // const addToBasket = (name) => {
    //     setBasket(current => {
    //         const index = current.findIndex(({name}) => {
    //             return currentName === name
    //         })
    //
    //         if (index === -1) {
    //             const newState = [...current, {name: currentName, count: 1}]
    //             return newState
    //         }
    //
    //         const newState = [...current];
    //         newState[index].count = current[index].count + 1;
    //         return newState
    //
    //     })
    //
    //     setIsOpenModal(false)
    // }


    // const deleteFromBasket = (currentName) => {
    //     setBasket(current => {
    //         const index = current.findIndex(({name}) => {
    //             return currentName === name
    //         })
    //
    //         const newState = [...current];
    //         newState.splice(index, 1)
    //         return newState
    //     })
    //     setIsOpenModal(false)
    // }


    // const toggleFav = (currentName) => {
    //     const index = items.findIndex(({name}) => {
    //         return currentName === name;
    //     });
    //
    //     setItems((current) => {
    //         const newState = [...current];
    //         newState[index].isFavourite = !current[index].isFavourite;
    //         let like = newState.filter(item => item.isFavourite)
    //         saveDataToLS(newState, basket)
    //         return newState;
    //     })
    // }


  return (
      <Provider store={store}>
        <BrowserRouter>

            <div className={styles.app}>
              <Header/>
                <section>
                    <Routes/>
                    <Modal/>
                </section>
            </div>

        </BrowserRouter>
      </Provider>
  );
}

export default App;
