import React from 'react';
import {Form, Formik, Field, ErrorMessage} from "formik";
import {Button} from "@mui/material";
import CustomField from "../CustomField/CustomField";
import { removeAllCartItems } from "../../store/actionCreators";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import * as yup from 'yup';

const Order = () => {

    const dispatch = useDispatch()
    const state = useSelector(state => state.cart.cartItems, shallowEqual)

    const initialValues = {
        name: '',
        secondName: '',
        age: '',
        address: '',
        telephone: '',
    }

    const validationSchema = yup.object().shape({
        name: yup.string().required("Field is require"),
        secondName: yup.string().required("Field is require"),
        age: yup.number().required("Field is require"),
        address: yup.string().required("Field is require"),
        telephone: yup.number().required("Field is require"),
    });

    const handleSubmit = (values) => {
        console.table(state)
        console.table(values)
        dispatch(removeAllCartItems(state))
    }

    return (
        <Formik
            initialValues={initialValues}
            onSubmit={handleSubmit}
            validationSchema={validationSchema}>
            {(props) => {
                return(
                    <Form style = {{display: 'flex', flexDirection: 'column', rowGap: '20px', padding: '30px', width: '50%', margin: '0 auto'}}>
                        <h2>Fill your order</h2>
                        <Field style = {{padding: '10px'}}
                               name = "name"
                               type = "text"
                               placeholder = "Name"
                               component = {CustomField}
                        />

                        <Field style = {{padding: '10px'}}
                               name = "secondName"
                               type = "text"
                               placeholder = "Second Name"
                               component = {CustomField}
                        />

                        <Field style = {{padding: '10px'}}
                               name = "age"
                               type = "text"
                               placeholder = "Age"
                               component = {CustomField}
                        />


                        <Field style = {{padding: '10px'}}
                               name = "address"
                               type = "text"
                               placeholder = "Address"
                               component = {CustomField}
                        />

                        <Field style = {{padding: '10px'}}
                               name = "telephone"
                               type = "text"
                               placeholder = "Telephone"
                               component = {CustomField}
                        />

                        <Button variant="contained" type={"submit"} color="success">Submit</Button>
                    </Form>
                )
            }}

        </Formik>
    );
};

export default Order;