import React from 'react';
import styles from './HomePage.module.scss'
import Item from "../../components/Item";

const HomePage = (props) => {
    const { items, openModal, toggleFav } = props;

    return (

        <section className={styles.root} >
        <div className={styles.container}>
            {items.map(elem => {
                return <Item
                    openModal={openModal}
                    key={elem.id}
                    name={elem.name}
                    color={elem.color}
                    id={elem.id}
                    url={elem.url}
                    article={elem.article}
                    price={elem.price}
                    isFavourite={elem.isFavourite}
                    toggleFav={toggleFav}
                />
            })}
        </div>
        </section>
    );
};

export default HomePage;