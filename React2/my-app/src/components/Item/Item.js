import React from 'react';
import styles from './Item.module.scss';
import { ReactComponent as StarRemove } from "../../assets/svg/star-remove.svg";
import { ReactComponent as StarAdd } from "../../assets/svg/star-plus.svg";
import PropTypes from 'prop-types';



const Item = (props) => {
    const { name, price, url, addItem, toggleFav, isFavourite, article, color } = props;
    console.log(props)

    return (
        <div className={styles.root}>
            <div className={styles.favourites} onClick={() => toggleFav(name)}>
                {isFavourite && <StarRemove/>}
                {!isFavourite && <StarAdd/>}
            </div>
            <p>{ name }</p>
            <img src={url} alt={name} />
            <span>price: {price}$ </span>
            <span>article: {article} </span>
            <span>color: {color} </span>

            <button onClick={() => {addItem(name, price)}} >Add to cart</button>
        </div>
    )
}

Item.propType = {
    name: PropTypes.string,
    price: PropTypes.string,
    url: PropTypes.string,
    addItem: PropTypes.func,
    toggleFav: PropTypes.func,
    isFavourite: PropTypes.bool,
    article: PropTypes.string,
    color: PropTypes.string,
}



export default Item;