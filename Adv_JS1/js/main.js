class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this._salary = salary;
  }

  set salary(value) {
    this._salary = value;
  }

  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const simon = new Programmer("Simon", 27, 2000, "Ukrainian");
const alex = new Programmer("Alex", 28, 1000, "Russian");
const pier = new Programmer("Pier", 29, 3000, "English");

console.log(simon);
console.log(alex);
console.log(pier);
