const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },

  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const list = document.createElement("ul");
const container = document.querySelector("#root").append(list);

books.forEach((element) => {
  const listItem = document.createElement("li");
  list.append(listItem);

  try {
    if (!element.author) {
      throw new Error("Не указан автор");
    }
    if (!element.name) {
      throw new Error("Не указано название");
    }
    if (!element.price) {
      throw new Error("Не указана цена");
    }

    listItem.append(
      `author: ${element.author}; name: ${element.name}; price: ${element.price}`
    );
  } catch (err) {
    console.log(err);
  }
});
