import BasketItem from "./BasketItem";
import { render } from '@testing-library/react';
import { Provider } from "react-redux";
import store from "../../store/index";


const Component = () => {

    return (
        <Provider store={store} >
            <BasketItem />
        </Provider>
    )
}

describe('BasketItem snapshot render', () => {
    test('should basketItem render', () => {
        const { asFragment } = render(<Component />);

        expect(asFragment()).toMatchSnapshot();
    })
})
