//////////////////////////////////////////////////////// CARDS
export const INIT_CARDS = 'INIT_CARDS';
export const IS_FAVORITE = 'IS_FAVORITE';


//////////////////////////////////////////////////////// CART
export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';


////////////////////////////////////////////////////////MODAL
export const SET_IS_OPEN = 'SET_IS_OPEN';
export const SET_MODAL_PARAMS = 'SET_MODAL_PARAMS';
