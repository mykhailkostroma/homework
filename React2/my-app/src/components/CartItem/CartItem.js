import React from 'react';
import styles from './CartItem.module.scss';
import PropTypes from 'prop-types';


const CartItem = (props) => {
    const { name, count } = props;

    return (
        <div className={styles.root}>
            <div>
                <span>{name}</span>
            </div>
            <span>{count}</span>
        </div>
    )
}

CartItem.propType = {
    name: PropTypes.string,
    count: PropTypes.number,
}



export default CartItem;