import React, {Component} from "react";
import styles from "./Modal.module.css"

class Modal extends Component {
    render() {

        const { closeModal, header, text, action } = this.props
        return(
            <div>
                <div className={styles.background} onClick={closeModal}></div>
                <div className={styles.modal}>
                    <button className={styles.cross} onClick={closeModal}>X</button>
                    <div className={styles.header}>{header}</div>
                    <div>{text}</div>
                    <div>{action}</div>
                </div>
            </div>
        )
    }
}

export default Modal;