import Modal from './Modal';
import { render } from '@testing-library/react';
import { Provider } from "react-redux";
import store from "../../store/index";


const Component = () => {

   return (
      <Provider store={store} >
         <Modal />
      </Provider>
   )
}

describe('Modal snapshot render', () => {
   test('should modal render', () => {
      const { asFragment } = render(<Component />);

      expect(asFragment()).toMatchSnapshot();
   })
})


