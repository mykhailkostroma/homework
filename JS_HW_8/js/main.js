"use strict";
console.dir(window);

let inputIn = document.querySelector(".input-in");
let error = document.createElement("div");
error.textContent = "Please enter correct price";
let divBeforeSpan = document.createElement("div");
let spanValue = document.createElement("span");
let buttonX = document.createElement("button");
buttonX.textContent = `x`;
let img = document.createElement("img");

inputIn.onblur = function onblur() {
  if (
    !inputIn.value ||
    isNaN(inputIn.value) ||
    inputIn.value.trim() === "" ||
    inputIn.value <= 0 ||
    typeof +inputIn.value !== "number"
  ) {
    inputIn.style.borderColor = "red";
    inputIn.after(error);
    error.after(img);
    img.src = "https://i.gifer.com/3b5c.gif";
  } else {
    inputIn.before(divBeforeSpan);
    divBeforeSpan.append(spanValue);
    spanValue.append(`Текущая цена: ${inputIn.value}`);
    spanValue.append(buttonX);
    inputIn.style.color = "green";
    error.innerHTML = "";
    img.src = "";
  }
};

const onfocusFun = function () {
  inputIn.style.borderColor = "green";
};

inputIn.addEventListener("focus", onfocusFun);

buttonX.onclick = function () {
  spanValue.innerHTML = "";
  inputIn.value = "";
  divBeforeSpan.append(img);
  img.src = "https://i.gifer.com/5Sb.gif";
};
