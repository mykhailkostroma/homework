import React from 'react';
import styles from './Modal.module.scss';
import { Button } from "@mui/material";
import PropTypes from 'prop-types';


const Modal= ({ actions, isOpen, setIsOpen }) => {

    if (!isOpen) return null;

        return (
             <div className={styles.root}>
                 <div className={styles.background} />
                 <div className={styles.content}>
                     <div className={styles.closeWrapper}>
                         <Button onClick={() => setIsOpen(false)} variant="text" color="warning">Close</Button>
                     </div>
                     <h2>Item has been added</h2>
                     <div className={styles.buttonContainer}>{actions}</div>
                 </div>
             </div>
        );
};

Modal.propTypes = {
    actions: PropTypes.element,
    isOpen: PropTypes.bool,
    setIsOpen: PropTypes.func,
}

export default Modal;
