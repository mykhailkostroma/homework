import './App.module.scss';
import styles from './App.module.scss'
import {useEffect, useState} from "react";
import ItemsContainer from "./components/ItemsContainer";
import Cart from "./components/Cart";
import Modal from "./components/Modal";
import {Button} from "@mui/material";


function App() {

    const [fruits, setFruits] = useState([])
    const [cartItems, setCartItems] = useState([])
    const [isOpen, setIsOpen] = useState(false);

    const openModal = () => {
        setIsOpen(true)
    }



    useEffect(() => {
            (async () => {
                const { data } = await fetch("./fruits.json").then(res => res.json())
               setFruits(data)
            })()
        }, [])


    const toggleFav = (name) => {
        const index = fruits.findIndex(({name: arrayName}) => {
            return name === arrayName;
        });

        setFruits((current) => {
            const newState = [...current];
            newState[index].isFavourite = !current[index].isFavourite;
            localStorage.setItem("isFavourite", JSON.stringify(newState));
            return newState;

        })
    }

    const addItem = (name, price) => {
        const index = cartItems.findIndex(({name: arrayName}) => {
            return name === arrayName;
        })
        if (index === -1) {
            setCartItems((current) => [...current, {name, price, count: 1}]
            )
        } else {
            setCartItems((current) => {
                const newState = [...current];
                newState[index].count = newState[index].count +1;
                localStorage.setItem("Cart", JSON.stringify(newState));
                return newState;


            })
        }

        openModal()
    }


  return (
    <div className={styles.root}>

        <div>
            <ItemsContainer toggleFav={toggleFav} items={fruits} addItem={addItem} />
        </div>

        <Modal
            isOpen={isOpen}
            setIsOpen={setIsOpen}
            actions={<Button variant="contained" onClick={() => setIsOpen(false)}>OK</Button>}/>

        <div>
            <Cart items={cartItems}/>
        </div>

    </div>
  );
}


export default App;
