import React from 'react';
import styles from './Header.module.scss'
import { NavLink } from "react-router-dom";


const Header = () => {
    return (
        <header className={styles.root}>

            <nav>
                <ul>
                    <li>
                        <NavLink exact activeClassName={styles.active} to='/'>Home</NavLink>
                    </li>
                    <li>
                        <NavLink activeClassName={styles.active} to='/basket'>Basket</NavLink>
                    </li>
                    <li>
                        <NavLink activeClassName={styles.active} to='/like'>Like</NavLink>
                    </li>
                </ul>
            </nav>

        </header>
    );
};

export default Header;