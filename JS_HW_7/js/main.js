"use strict";
console.dir(window);

function createList(array, parent = document.body) {
  const list = array.map((element) => `<li>${element}</li>`);
  parent.insertAdjacentHTML("afterbegin", `<ul> ${list.join(" ")}</ul>`);
}

createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
