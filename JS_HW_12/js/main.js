"use strict";
console.dir(window);

const imgs = document.querySelectorAll(".image-to-show");
const stopBtn = document.querySelector(".stop-btn");
const startBtn = document.querySelector(".start-btn");

let n = 0;
let i = 0;

function imgSlider() {
  imgs.forEach((elem) => elem.classList.remove("active"));
  imgs[n].classList.add("active");
  n++;
  if (n === imgs.length) {
    n = 0;
  }
}

function start() {
  if (i == 0) {
    i = setInterval(imgSlider, 3000);
  }
}

stopBtn.onclick = function stop() {
  clearInterval(i);
  i = 0;
};
startBtn.onclick = function () {
  start();
};
window.onload = function () {
  start();
};
