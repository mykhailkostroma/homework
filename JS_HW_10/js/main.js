"use strict";
console.dir(window);

const eye = document.querySelectorAll(".fas.fa-eye.icon-password");
const eyeSlash = document.querySelectorAll(".fas.fa-eye-slash.icon-password");
const passOne = document.querySelector(".password-one");
const passTwo = document.querySelector(".password-two");
const submit = document.querySelector(".btn");

eyeSlash.forEach((item) => {
  item.style.display = "none";
});

const eyeOne = eye[0];
const eyeTwo = eye[1];
const eyeSlashOne = eyeSlash[0];
const eyeSlashTwo = eyeSlash[1];

eyeOne.addEventListener("click", function (event) {
  eyeSlashOne.style.display = "block";
  passOne.type = "text";
});

eyeSlashOne.addEventListener("click", function (event) {
  eyeSlashOne.style.display = "none";
  passOne.type = "password";
});

eyeTwo.addEventListener("click", function (event) {
  eyeSlashTwo.style.display = "block";
  passTwo.type = "text";
});

eyeSlashTwo.addEventListener("click", function (event) {
  eyeSlashTwo.style.display = "none";
  passTwo.type = "password";
});

submit.addEventListener("click", function (event) {
  if (passOne.value === passTwo.value) {
    alert("You are welcome");
  } else {
    const div = document.createElement("div");
    div.textContent = "Нужно ввести одинаковые значения";
    div.style.color = "red";
    passTwo.after(div);
  }
});
