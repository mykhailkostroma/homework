import React from 'react';
import styles from './LikePage.module.scss'
import Item from "../../components/Item";
import {shallowEqual, useSelector} from "react-redux";
import {getFromLS} from "../../utils/localStorage";
import item from "../../components/Item";

const LikePage = () => {

    const {cards} = useSelector(state => state.cards, shallowEqual)
    const fevCards = getFromLS('cards').filter( item => item.isFavourite);


    return (

        <section className={styles.root}>
        <div className={styles.container}>
            {fevCards.length > 0 && fevCards.map(item => {
                return <Item key={item.id} {...item} />

            })}
            {!fevCards.length && <h1>No items have been added :(</h1>}
        </div>
        </section>
    );
};

export default LikePage;