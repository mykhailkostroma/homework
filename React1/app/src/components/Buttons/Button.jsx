import React, {Component} from "react";
import styles from "./Button.module.css"

class Button extends Component {

    render() {


        const { openFirstModal, openSecondModal, backgroundColor, textFirstBtn, textSecondBtn } = this.props
        console.log(this.props)

        return(
            <div className={styles.openModal}>
                <button type={"button"} onClick={openFirstModal} style={{backgroundColor}} className={styles.openModalBtn}>{textFirstBtn}</button>
                <button type={"button"} onClick={openSecondModal} style={{backgroundColor}} className={styles.openModalBtn}>{textSecondBtn}</button>
            </div>

        )

    }
}

export default Button;