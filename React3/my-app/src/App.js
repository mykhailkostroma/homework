import styles from './App.module.scss';
import Header from "./components/Header/Header";
import {BrowserRouter} from "react-router-dom";
import {useEffect, useState} from "react";
import Routes from "./Routes";
import Modal from "./components/Modal";
import {Button} from "@mui/material";

function App() {

    const [items, setItems] = useState([]);
    const [isOpenModal, setIsOpenModal] = useState(false);
    const [currentName, setCurrentName] = useState("");
    const [basket, setBasket] = useState([]);
    const [isAddModal, setIsAddModal] = useState(true);

    useEffect(() => {

        const LSData = localStorage.getItem("data");

        if (LSData) {
            const parsedData = JSON.parse(LSData);
            setItems(parsedData?.items || '');
            setBasket(parsedData?.basket || '');
            return;
        }

        (async () => {
            const { data } = await fetch('./fruits.json').then(res => res.json())
            setItems(data)
        })()
    }, []);


    const saveDataToLS = (items, basket) => {
        localStorage.setItem("data", JSON.stringify({
            items,
            basket,
        }))
    }


    const openModal = (name, type = 'add') => {
        if (type === 'delete') {
            setIsAddModal(false);
        } else {
            setIsAddModal(true);
        }

        setIsOpenModal(true)
        setCurrentName(name)
    }

    const addToBasket = (name) => {
        setBasket(current => {
            const index = current.findIndex(({name}) => {
                return currentName === name
            })

            if (index === -1) {
                const newState = [...current, {name: currentName, count: 1}]
                saveDataToLS(items, newState);
                return newState
            }

            const newState = [...current];
            newState[index].count = current[index].count + 1;
            saveDataToLS(items, newState);
            return newState

        })

        setIsOpenModal(false)
    }


    const deleteFromBasket = (currentName) => {
        setBasket(current => {
            const index = current.findIndex(({name}) => {
                return currentName === name
            })

            const newState = [...current];
            newState.splice(index, 1)
            saveDataToLS(items, newState);
            return newState
        })
        setIsOpenModal(false)
    }


    const toggleFav = (currentName) => {
        const index = items.findIndex(({name}) => {
            return currentName === name;
        });

        setItems((current) => {
            const newState = [...current];
            newState[index].isFavourite = !current[index].isFavourite;
            let like = newState.filter(item => item.isFavourite)
            saveDataToLS(newState, basket)
            return newState;
        })
    }


  return (
      <BrowserRouter>
        <div className={styles.app}>
          <Header/>

            <section>
                <Routes items={items} openModal={openModal} addToBasket={addToBasket} basket={basket} toggleFav={toggleFav}/>
                <Modal isOpenModal={isOpenModal} setIsOpenModal={setIsOpenModal} title={currentName}
                actions={
                    <>
                        <Button variant="contained" onClick={ () => {

                            if (isAddModal) {
                                addToBasket(currentName)
                            } else {
                                deleteFromBasket(currentName)
                            }

                        }}>OK</Button>

                        <Button variant="contained" color="error" onClick={ () => setIsOpenModal(false)}>NO</Button>
                    </>
                }
                />
            </section>



        </div>
      </BrowserRouter>
  );
}

export default App;
