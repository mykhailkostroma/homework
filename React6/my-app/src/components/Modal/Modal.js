import React from 'react';
import styles from './Modal.module.scss';
import {Button} from "@mui/material";
import { useDispatch, useSelector } from 'react-redux';
import {setIsOpenModal, removeCartItem} from "../../store/actionCreators";

const Modal= () => {
    const {isOpen, id, name} = useSelector(state => state.modal);
    // console.log(id)
    const dispatch = useDispatch();
    const closeModal = () => dispatch(setIsOpenModal(false));
    const removeItem = () => dispatch(removeCartItem(id));

    if (!isOpen) return null;

    return (
         <div className={styles.root}>
             <div className={styles.background} onClick={closeModal}/>
             <div className={styles.content}>
                 <div className={styles.closeWrapper}>
                     <Button onClick={closeModal} className={styles.btn} variant="contained" color="error">X</Button>
                 </div>
                 <h2>Are you seriously want to delete {name}?</h2>
                 <div className={styles.buttonContainer}>
                     <Button variant="contained" color="error" onClick={() => {
                         dispatch(removeCartItem(id));
                         closeModal()
                     }}>Yes</Button>
                     <Button onClick={closeModal} variant="contained">No</Button>
                 </div>
             </div>
         </div>
    );
};

export default Modal;
