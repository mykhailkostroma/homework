import React from 'react';
import {TextField} from "@mui/material";

const CustomField = (props) => {
    const {type, placeholder, field, form} = props;
    const isError = form.touched[field.name] && form.errors[field.name];
    return (
        <>
            <TextField
                color={isError ? "error" : "primary"}
                style={{zIndex:'0'}}
                id="outlined-basic"
                label={placeholder}
                variant="outlined"
                type={type}
                placeholder={placeholder}
                {...field}
            />

            {isError ? <span style={{color: "tomato"}}>{form.errors[field.name]}</span> : ""}
        </>
    );
};

export default CustomField;