import React from 'react';
import styles from './LikePage.module.scss'
import Item from "../../components/Item";

const LikePage = (props) => {

    const {items, openModal, toggleFav} = props

    return (

        <section className={styles.root}>
        <div className={styles.container}>
            {items.map(elem => {
                return elem.isFavourite && <Item
                    key={elem.id}
                    name={elem.name}
                    color={elem.color}
                    id={elem.id}
                    url={elem.url}
                    article={elem.article}
                    price={elem.price}
                    isFavourite={elem.isFavourite}
                    toggleFav={toggleFav}
                    openModal={openModal}/>
            })}
        </div>
        </section>
    );
};

export default LikePage;