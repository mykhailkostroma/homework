import Item from "./Item";
import { render } from '@testing-library/react';
import { Provider } from "react-redux";
import store from "../../store/index";


const Component = () => {

    return (
        <Provider store={store} >
            <Item />
        </Provider>
    )
}

describe('Item snapshot render', () => {
    test('should item render', () => {
        const { asFragment } = render(<Component />);

        expect(asFragment()).toMatchSnapshot();
    })
})
