import React from 'react';
import styles from './ItemsContainer.module.scss';
import Item from "../Item";
import PropTypes from 'prop-types';

const ItemsContainer = (props) => {
    const {items, addItem, toggleFav} = props;

    return (
        <section className={styles.root}>
            <h1>ITEMS</h1>
            <div className={styles.container}>
                {items && items.map(item => <Item key={item.id} toggleFav={toggleFav} {...item} addItem={addItem} />)}
            </div>
        </section>
    )
}

ItemsContainer.propType = {
    items: PropTypes.array,
    addItem: PropTypes.func,
    toggleFav: PropTypes.func
}

export default ItemsContainer;