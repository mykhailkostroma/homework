import React from 'react';
import {Route, Switch} from "react-router-dom";
import HomePage from "./pages/HomePage";
import BasketPage from "./pages/BasketPage";
import LikePage from "./pages/LikePage";

const Routes = (props) => {

    const { items, openModal, basket, toggleFav } = props
    return (
       <Switch>
           <Route exact path='/'>
               <HomePage items={items} openModal={openModal} toggleFav={toggleFav}/>
           </Route>


           <Route exact path='/basket'>
               <BasketPage basket={basket} openModal={openModal}/>
           </Route>


           <Route exact path='/like'>
               <LikePage items={items} openModal={openModal} toggleFav={toggleFav}/>
           </Route>
       </Switch>
    );
};

export default Routes;