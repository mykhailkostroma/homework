import React from 'react';
import styles from './BasketItem.module.scss'
import { Button } from "@mui/material";
import {useDispatch} from "react-redux";
import {setIsOpenModal, setModalParams} from "../../store/actionCreators";

const BasketItem = (props) => {
    const {name, count, id} = props;
    const dispatch = useDispatch();

    const handleDelete = () => {
        dispatch(setModalParams({ name, id }));
        dispatch(setIsOpenModal(true));
    }

    return (
        <div className={styles.countItem}>
            <div className={styles.wrapper}>
                <span>{name}</span>
            </div>

            <div className={styles.wrapper}>
                <span>{count}</span>
                <Button variant="contained" color='error' onClick={handleDelete}>Delete</Button>
            </div>
        </div>
    );
};

export default BasketItem;