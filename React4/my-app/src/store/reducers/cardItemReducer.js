import React from 'react';
import {INIT_CARDS, IS_FAVORITE} from "../actions";
import {saveToLS} from "../../utils/localStorage";

const initialValue = {
    cards: [],
}

const CardItemReducer = (state = initialValue, action) => {
    switch (action.type) {
        case INIT_CARDS:
            saveToLS('cards', action.payload)
            return {...state, cards: action.payload}

        case IS_FAVORITE:
            const newCards = [...state.cards];
            const index = newCards.findIndex(elem => elem.id === action.payload.id)
            newCards[index].isFavourite = !newCards[index].isFavourite
            saveToLS('cards', newCards)
            return {...state, cards: newCards}

        default:
            return state
    }
};

export default CardItemReducer;