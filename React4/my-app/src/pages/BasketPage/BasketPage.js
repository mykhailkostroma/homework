import React from 'react';
import styles from './BasketPage.module.scss'
import BasketItem from "../../components/BasketItem";
import {shallowEqual, useSelector} from "react-redux";

const BasketPage = () => {

    const basketItems = useSelector(state => state.cart.cartItems, shallowEqual)

    return (
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {basketItems && basketItems.map(item => <BasketItem key={item.id} {...item}/>)}
            {!basketItems.length && <h1>No items have been added :(</h1>}
        </div>
    );
};

export default BasketPage;