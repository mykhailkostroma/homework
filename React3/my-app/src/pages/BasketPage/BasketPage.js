import React from 'react';
import styles from './BasketPage.module.scss'
import BasketItem from "../../components/BasketItem";

const BasketPage = (props) => {

    const { basket, openModal } = props;

    return (
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {basket.map(elem => {

                return <BasketItem key={elem.name} count={elem.count} name={elem.name} openModal={openModal}/>
            })}
        </div>
    );
};

export default BasketPage;