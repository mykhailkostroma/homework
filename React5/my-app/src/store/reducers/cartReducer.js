import React from 'react';
import {ADD_ITEM, REMOVE_ITEM, REMOVE_ALL_ITEMS} from "../actions";
import {saveToLS, getFromLS} from "../../utils/localStorage";

const initialState = {
    cartItems: getFromLS('cart') || [],
}

const CartReducer = (state = initialState, action) => {
    switch (action.type) {

        case ADD_ITEM: {
            const newCartItems = [...state.cartItems];
            const index = newCartItems.findIndex(item => item.id === action.payload.id);

            if (index === -1) {
                const newItem = {...action.payload, count: 1}
                saveToLS('cart', [...state.cartItems, newItem])
                return {...state, cartItems: [...state.cartItems, newItem]}
            }

            newCartItems[index].count = newCartItems[index].count + 1;
            saveToLS('cart', newCartItems);
            return {...state, cartItems: newCartItems}
        }

        case REMOVE_ITEM: {
            const newCartItems = [...state.cartItems];
            const index = newCartItems.findIndex(item => item.id === action.payload);

            if (index === -1) {
                return {...state}
            }

            newCartItems.splice(index, 1)
            saveToLS('cart', newCartItems);
            return {...state, cartItems: newCartItems}
        }

        case REMOVE_ALL_ITEMS: {
            localStorage.removeItem('cart');
            return {cartItems: []}
        }

        default:
            return state
    }

};

export default CartReducer;