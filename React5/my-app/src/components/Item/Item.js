import React from 'react';
import styles from './Item.module.scss';
import { ReactComponent as StarRemove } from "../../assets/svg/star-remove.svg";
import { ReactComponent as StarAdd } from "../../assets/svg/star-plus.svg";
import PropTypes from 'prop-types';
import {Button} from "@mui/material";
import {useDispatch} from "react-redux";
import {addCartItem, setIsFavourite} from "../../store/actionCreators";



const Item = (props) => {
    const { name, price, url, isFavourite, article, color, id } = props;
    const dispatch = useDispatch();
    // const fav = useSelector(state => state.cards)



    return (
        <div className={styles.root}>
            <div className={styles.favourites} onClick={() => dispatch(setIsFavourite({id}))}>
                {isFavourite && <StarRemove/>}
                {!isFavourite && <StarAdd/>}
            </div>
            <p>{ name }</p>
            <img src={url} alt={name} />
            <span>price: {price}$ </span>
            <span>article: {article} </span>
            <span>color: {color} </span>
            <Button variant="outlined" size={"large"} onClick={() => {
                dispatch(addCartItem({name, id}))
            }}>BASKET</Button>
        </div>
    )
}

Item.propType = {
    name: PropTypes.string,
    price: PropTypes.string,
    url: PropTypes.string,
    addItem: PropTypes.func,
    toggleFav: PropTypes.func,
    isFavourite: PropTypes.bool,
    article: PropTypes.string,
    color: PropTypes.string,
}



export default Item;