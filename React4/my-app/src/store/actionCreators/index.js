import {INIT_CARDS, IS_FAVORITE, ADD_ITEM, REMOVE_ITEM, SET_MODAL_PARAMS, SET_IS_OPEN} from "../actions";


/////////////////////////////////////////////////////////////////CARDS
export const initCardItem = () => async (dispatch) => {
    const { data } = await fetch('./fruits.json').then(res => res.json())
    dispatch({type: INIT_CARDS, payload: data})
}

export const setIsFavourite = (id) => ({
    type: IS_FAVORITE,
    payload: id
})


/////////////////////////////////////////////////////////////////CART
export const addCartItem = (cartItem) => ({
    type: ADD_ITEM,
    payload: cartItem
});

export const removeCartItem = (id) => ({
    type: REMOVE_ITEM,
    payload: id
});


/////////////////////////////////////////////////////////////////MODAL
export const setIsOpenModal = (value) => ({ type: SET_IS_OPEN, payload: value });
export const setModalParams = (value) => ({ type: SET_MODAL_PARAMS, payload: value });
