"use strict";
console.dir(window);

const firstName = prompt("Enter you name: ", "");
const lastName = prompt("Enter you second name: ", "");
const birthday = prompt("Enter you age: ", "dd.mm.yyyy");

function CreateNewUser(name, secondName, birthday) {
  const newUser = {
    name,
    secondName,
    birthday,
    getAge() {
      return new Date().getFullYear() - this.birthday.slice(-4);
    },
    getLogin() {
      return (this.name.charAt(0) + this.secondName).toLowerCase();
    },
    getPassword() {
      return (
        this.name.charAt(0).toUpperCase() +
        this.secondName.toLowerCase() +
        this.birthday.slice(-4)
      );
    },
  };
  return newUser;
}

console.log(CreateNewUser(firstName, lastName, birthday).getAge());
console.log(CreateNewUser(firstName, lastName, birthday).getPassword());
