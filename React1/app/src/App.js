import './App.css';
import React, {Component} from "react";
import Button from "./components/Buttons/Button";
import Modal from "./components/Modal/Modal";


class App extends Component {

    state = {
        isOpenFirstModal: false,
        isOpenSecondModal: false,
    }

    openFirstModal = () => {
        this.setState({
            isOpenFirstModal: true,
            isOpenSecondModal: false,
        })

    };

    openSecondModal = () => {
        this.setState({
            isOpenSecondModal: true,
            isOpenFirstModal: false,
        })

    };

    closeModal = () => {
        this.setState({
            isOpenFirstModal: false,
            isOpenSecondModal: false,
        })
    };


  render() {
    return(

        <div>
           <Button openFirstModal = {this.openFirstModal}
                   openSecondModal = {this.openSecondModal}
                   backgroundColor = "tomato"
                   textFirstBtn = "Open first modal"
                   textSecondBtn = "Open second modal"  />

            {this.state.isOpenFirstModal &&
            <Modal
                closeModal = {this.closeModal}
                header = "Do you want delete this file?"
                text = {<p>Once you delete this file, it won't be possible to undo this action.<br/>Are you sure you want delete it?</p>}
                action = {<button>Accept</button>}
            />}

            {this.state.isOpenSecondModal &&
            <Modal
                closeModal = {this.closeModal}
                header = "Modal number 2"
                text = "Main text for modal 2"
                action = {<button>Cancel</button>}
            />}
        </div>

    )

  }
}

export default App;
