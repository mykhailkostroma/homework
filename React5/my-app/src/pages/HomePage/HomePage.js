import React, {useEffect} from 'react';
import styles from './HomePage.module.scss'
import Item from "../../components/Item";
import {useDispatch, useSelector, shallowEqual} from "react-redux";
import {initCardItem} from "../../store/actionCreators";
import {getFromLS} from "../../utils/localStorage";
import {INIT_CARDS} from "../../store/actions";

const HomePage = () => {

    const {cards} = useSelector(state => state.cards, shallowEqual)
    const dispatch = useDispatch()

    useEffect(() => {
        let cardsLS = getFromLS('cards');
        if (cardsLS) {
            dispatch({ type: INIT_CARDS, payload: cardsLS })
        }
        else dispatch(initCardItem())
    }, [])

    return (

        <section className={styles.root}>
        <div className={styles.container}>
            {cards.length && cards.map(item => <Item key={item.id} {...item}/>)}
        </div>
        </section>
    );
};

export default HomePage;