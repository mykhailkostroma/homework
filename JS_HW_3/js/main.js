"use strict";
console.dir(window);

let num1 = +prompt("fill a first number");
let num2 = +prompt("fill a second number");
let op = prompt("choose operation: + or - or * or /");

function mathResult(num1, num2, op) {
  switch (op) {
    case "+":
      return num1 + num2;
    case "-":
      return num1 - num2;
    case "*":
      return num1 * num2;
    case "/":
      if (num2) {
        return num1 / num2;
      } else {
        return "infinity";
      }
  }
}
console.log(mathResult(num1, num2, op));

// Вариант через eval

// let num1 = +prompt("fill a first number");
// let num2 = +prompt("fill a second number");
// let op = prompt("choose operation: + or - or * or /");

// function mathResult(num1, num2, op) {
//   return eval(`${num1}` + op + `${num2}`);
// }

// console.log(mathResult(num1, num2, op));
