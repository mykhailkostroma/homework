import React, {useEffect, useState} from 'react';
import styles from './Cart.scss';
import CartItem from "../CartItem";
import PropTypes from "prop-types";

const Cart = (props) => {
    const { items } = props;
    const [sum, setSum] = useState(0);

    useEffect(()=> {
        let sum = 0;
        items.forEach(e => sum += e.price * e.count);
        setSum(sum);
    }, [items])

    return (
        <section className={styles.root}>
            <h2>CART</h2>
            <div className={styles.items}>
                {items && items.map((item, i) => <CartItem key={i} {...item} />)}
            </div>
            <p>Price: {sum}$</p>
        </section>
    )
}

Cart.propType = {
    items: PropTypes.array
}

export default Cart;