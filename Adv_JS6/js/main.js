const ip = async () => {
    const {status, data} = await axios.get("https://api.ipify.org/?format=json")

   if (status === 200) {
       const result = await axios.get(`http://ip-api.com/json/${data.ip}?fields=continent,country,regionName,city,district`)

       const ipAdress = JSON.stringify(result.data)

       document.querySelector("button").addEventListener('click', () =>{

           if (!document.querySelector("p")) {
               document.querySelector(".container").insertAdjacentHTML("beforeend",
                   ` <p>${ipAdress}</p>`)
           } else {
               document.querySelector("p").remove()
               document.querySelector(".container").insertAdjacentHTML("beforeend",
                   ` <p>${ipAdress}</p>`)
           }

       })
   }
}



ip()
