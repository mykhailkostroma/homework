"use strict";
console.dir(window);

// function filterBy(array, string) {
//   const resultArray = [];
//   array.forEach((element) => {
//     if (typeof element !== string) {
//       resultArray.push(element);
//     }
//   });
//   return resultArray;
// }

// console.log(filterBy(["hello", "world", 23, "23", null], "string"));

function filterBy(array, type) {
  return array.filter((element) => typeof element !== type);
}

console.log(filterBy(["hello", "world", 23, "23", null], "string"));
