import { SET_IS_OPEN, SET_MODAL_PARAMS} from "../actions";


const initialValues = {
   isOpen: false,
   id: '',
   name: '',
}

const modalReducer = (state = initialValues, action) => {
    switch (action.type){
        case SET_IS_OPEN: {
            return {...state, isOpen: action.payload}
        }
        case SET_MODAL_PARAMS: {
            return {...state, name: action.payload?.name, id: action.payload?.id}
        }
       
        default:
            return state
    }

}

export default modalReducer;